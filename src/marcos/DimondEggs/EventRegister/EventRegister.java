package marcos.DimondEggs.EventRegister;

import java.util.Random;

import org.bukkit.Material;
import org.bukkit.entity.Chicken;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;


public class EventRegister implements Listener{
	
	
	public ItemStack diamante() {
		ItemStack dima = new ItemStack(Material.DIAMOND);
		
		return dima;
	}
	
	@EventHandler
	public void itemDrop(EntityDeathEvent drop) {
		
		Chicken chickLouco = (Chicken) drop.getEntity();
		if (chickLouco instanceof Chicken) {
			int rndom = new Random().nextInt(100);
			if (rndom >= 97) {
			chickLouco.getWorld().dropItemNaturally(chickLouco.getLocation(), diamante());
			}
		}
	}
}
